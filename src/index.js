import { Builder, parseString as _parseString } from 'xml2js';
import { find } from 'xml2js-xpath';

const parseString = _parseString;
const builder = new Builder();

export function insertURIs(xmlStr, annotations, callback) {
  parseString(xmlStr, (err, json) => {
    for (const annotation of annotations) {
      const entityXPath = annotation.target.selector['oa:value']['@value'];

      if (checkEmptyBody(annotation.body)) continue;

      const uri = getFirstUri(annotation.body)
      const matches = find(json, entityXPath);
      try {
        matches[0]['idno'] = { $: { 'sameAs': uri, 'type': getSource(uri) } };
      } catch (error) {
        console.error(`entity identified by XPath ${entityXPath} not found in XML`);
      }
    }

    callback(builder.buildObject(json));
  });
}

const getSource = (uri) => {
  if (uri.includes('viaf.org')) return 'VIAF';
  if (uri.includes('wikidata.org')) return 'WIKIDATA';

  return 'UNKNOWN';
};

const checkEmptyBody = (annotationBody) => {
  const items = annotationBody["as:items"];
  return Array.isArray(items) && items.length == 0;
};

const getFirstUri = (annotationBody) => {
  const items = annotationBody["as:items"];
  if (Array.isArray(items)) return items[0].id;

  return items.id;
};
