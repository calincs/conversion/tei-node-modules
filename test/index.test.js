import { strict as assert } from 'assert';
import { readFileSync } from 'fs';
import { insertURIs } from '../src/index.js';

describe('unit test', () => {
  it('should insert one URI into the XML', () => {
    let containerJSONRaw = readFileSync('test/container.test.json');
    let containerJSON = JSON.parse(containerJSONRaw);
    let testXML = "<TEI xmlns=\"http://www.tei-c.org/ns/1.0\"><text><body><div type=\"DigitalCraikTeam\"><listPerson><person xml:id=\"BourrierKaren\"><persName><surname>Bourrier</surname><forename>Karen</forename><roleName>Project Director</roleName></persName><occupation><affiliation>University of Calgary</affiliation></occupation></person></listPerson></div></body></text></TEI>";
    let targetXML =
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <text>
    <body>
      <div type="DigitalCraikTeam">
        <listPerson>
          <person xml:id="BourrierKaren">
            <persName>
              <surname>Bourrier</surname>
              <forename>Karen</forename>
              <roleName>Project Director</roleName>
            </persName>
            <occupation>
              <affiliation>University of Calgary</affiliation>
            </occupation>
            <idno sameAs="http://viaf.org/viaf/311459210/" type="VIAF"/>
          </person>
        </listPerson>
      </div>
    </body>
  </text>
</TEI>`

    insertURIs(testXML, containerJSON.first.items, (result) => {
      assert.strictEqual(result, targetXML);
    });
  });
});
